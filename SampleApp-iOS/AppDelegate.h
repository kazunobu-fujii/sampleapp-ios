//
//  AppDelegate.h
//  SampleApp-iOS
//
//  Created by FUJII Kazunobu on 2014/02/28.
//  Copyright (c) 2014年 FUJII Kazunobu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
